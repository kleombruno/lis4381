<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Kyle Leombruno">
	<link rel="icon" href="favicon.ico">

	<title>My Online Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	color: #666;
	padding-top: 50px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}
.item
{
	background: #333;    
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
}
.bs-example
{
  margin: 20px;
}
</style>

</head>
<body>

	<?php include_once("global/nav_global.php"); ?>
	
	<div class="container">
			<div class="starter-template">
						<div class="page-header">
							<?php include_once("global/header.php"); ?>	
						</div>

<!-- Start Bootstrap Carousel  -->
	<div class="bs-example">
	<div
      id="myCarousel"
		class="carousel"
		data-interval="5000"
		data-pause="hover"
		data-wrap="true"
		data-keyboard="true"			
		data-ride="carousel">
		
    	<!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
       <!-- Carousel items -->
        <div class="carousel-inner">

			<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
          <div class="active item">
						<div class="container">
							<h2>School</h2>
                <div class="carousel-caption">
                 <h3>My Major</h3>
								 <img src="a4/img/biglogo.png" alt="Slide 1">
                 <p>I am a senior at FSU majoring in ICT. I am facinsated by website building and programming but I beleive its even better as a team.</p> 
                </div>
            </div>
					</div>

            <div class="item">
                <h2>Work</h2>
                <div class="carousel-caption">
                  <h3>My Job</h3>
									<img src="a4/img/loftsmart.png" alt="Slide 2">	
                  <p>I am a marketing stratiegest for the start up student housing website, Loftsmart. My job is creatively market our website to FSU students and encourge them to use the website.</p>
						 								
                </div>
            </div>

            <div class="item">
                <h2>Leadership</h2>
                <div class="carousel-caption">
                  <h3>My Leadership Projects</h3>
									<img src="a4/img/slide3.jpg" alt="Slide 3">	
                  <p>All semester I have been working on promoting and supporting the Transfers Helping Transfers club at FSU. I have been working with the president of the club to decide the best ways to reach transfer student and then we put them to the test. So far, we have added over 30 likes to the page and added 10+ members. </p>
						 								
                </div>
            </div>

        </div>
					
	
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
				</div>
	</div>
	</div>
		
	
<!-- End Bootstrap Carousel  -->
						
<?php
include_once "global/footer.php"; ?>
?>

	</div> <!-- end starter-template -->
	</div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	
</body>
</html>
