<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("../global/header.php"); ?>	
				</div>
				
        <?php
  $url = 'http://rss.cnn.com/rss/edition_world.rss';
  $feed = simplexml_load_file($url, 'SimpleXMLIterator');
  echo "<h2>" .$feed->channel->description. "</h2><ol>";

  $filtered = new LimitIterator($feed->channel->item, 0, 10);
  foreach($filtered as $item) { ?>
   <h4><li><a href="<?= $item->link; ?>" target="_blank"><?= $item->title;?></a></li></h4> 
<?php
    date_default_timezone_set('America/New_York');
    
    $date = new DateTime($item->pubDate);
    $date->setTimezone(new DateTimeZone('America/New_York'));
    $offset = $date->getOffset();
    $timezone = ($offset == -14400) ? 'EDT' : 'EST';
    
    echo $date->format('M j, Y, g:ia') .$timezone;
?>

    <p><?php echo $item->description;?></p>
<?php } ?>
   </ol>
   
				<?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
