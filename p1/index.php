<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("../global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> I created an android app that has my headshot and name on the homepage. There is a button on the bottom that can bring you to my contact/personal information page. 
				</p>

				<h4>Profile Homepage</h4>
				<img src="img/profile.png" class="img-responsive center-block" alt="Profile Homepage">

				<h4>Personal Information</h4>
				<img src="img/info.png" class="img-responsive center-block" alt="Personal Information">

				
				<?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
