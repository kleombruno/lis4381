

# Lis 4381 - Mobile Web Applications Development

## Kyle Leombruno

### Lis 4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My first App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command description

2. [A2 README.md](a2/README.md)
    - Create main activity and recipe 
    - create a button that brings you to another page
    - add an image onto the home screen
    - push pictures up to bitbucket
   

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - create function to calculate ticket price
    - create drop down menu with four bands
    - add ticket price for each individual band and ticket
    - create ERD in SQL
    - add data into each table
    

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - fix the carousel on the homepage and add pictures and descriptions
    - add data all required fields for Pet Stores
    - add data validation so the boxes turn red when empty or the users input is incorrect
    

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - create a server side validation
    - create a table with all the entered valued
    - create a error page when validation is incorrect
 

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - create a main activity page with your headshot
    - create a page with all your personal and contact information
    - create a button to go to the personal information page
    - add borders around your headshot and button
    - add text shadow on the button
    
7. [P2 README.md](p2/README.md "My P2 README.md file")
    
    - add functionality to the delete button to delete records
    - add functionality to the edit button to update current records
    - add a RSS Feed to the portfolio
    
