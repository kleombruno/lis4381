#LIS4381

#Kyle Leombruno

#Assignment 4 Reqiurements

1. Make edit button able to update the database
2. Add functionality to the delete button
3. Add a RSS feed

#Read.ME File will include

* Screenshot of homepage
* Screenshot of database index
* Screenshot of editing fields
* Screenshot of error with server-side validation
* Screenshot of RSS Feed

#Assignment Screenshots

![My portfolio homepage](img/home.png)

![Project 2 Database](img/database.png)

![Editing current record to update](img/edit.png)

![Update Error](img/error.png)

![RSS Feed](img/rssfeed.png)


