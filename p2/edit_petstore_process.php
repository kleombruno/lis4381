<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//Get item data
//No need for pst_id when adding, uses auto incriment
$pst_id_v = $_POST['pst_id'];
$pst_name_v = $_POST['pst_name'];
$pst_street_v = $_POST['pst_street'];
$pst_city_v = $_POST['pst_city'];
$pst_state_v = $_POST['pst_state'];
$pst_zip_v = $_POST['pst_zip'];
$pst_phone_v = $_POST['pst_phone'];
$pst_email_v = $_POST['pst_email'];
$pst_url_v = $_POST['pst_url'];
$pst_ytd_sales_v = $_POST['pst_ytdsales'];
$pst_notes_v = $_POST['pst_notes'];

//exit($pst_name_v.",".$pst_city_v);

//Validation for Server side
//Name
$pattern='/^[a-zA-Z0-9,\-_\s]+$/';
$valid_name = preg_match($pattern, $pst_name_v);

//Street
$pattern='/^[a-zA-Z0-9,\-\s\.]+$/';
$valid_street = preg_match($pattern, $pst_street_v);
//City
$pattern='/^[a-zA-Z0-9,\s]+$/';
$valid_city = preg_match($pattern, $pst_city_v);
//State
$pattern='/^[a-zA-Z]{2}+$/';
$valid_state = preg_match($pattern, $pst_state_v);
//Zip
$pattern='/^\d{5,9}+$/';
$valid_zip = preg_match($pattern, $pst_zip_v);
//Phone
$pattern='/^\d{10}+$/';
$valid_phone = preg_match($pattern, $pst_phone_v);
//Email
$pattern='/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/';
$valid_email = preg_match($pattern, $pst_email_v);
//Url
$pattern='/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
$valid_url = preg_match($pattern, $pst_url_v);
//YtdSales
$pattern='/^[0-9\.]+$/';
$valid_ytdsales = preg_match($pattern, $pst_ytd_sales_v);

//echo $valid_name, $valid_city, $valid_email, $valid_phone, $valid_state, $valid_street, $valid_url, $valid_ytdsales, $valid_zip;
//exit();

if (
    empty($pst_name_v) ||
    empty($pst_street_v) ||
    empty($pst_city_v) ||
    empty($pst_state_v) ||
    empty($pst_zip_v) ||
    empty($pst_phone_v) ||
    empty($pst_email_v) ||
    empty($pst_url_v) ||
    empty($pst_ytd_sales_v)
){
    $error = "All feilds require data, except <b>Notes</b>. Check all feilds and try again.";
    include('global/error.php');
} 
else if (!is_numeric($pst_ytd_sales_v) || $pst_ytd_sales_v <= 0){
    $error = "YTD Sales can contain numbers and/or decimals";
    include('global/error.php');
}
else if ($valid_name === false){
    echo "Error in the pattern!";
}
else if ($valid_name === 0){
    $error = "Name can only contain letters";
    include('global/error.php');
}
else if ($valid_street === false){
    echo "Error in the pattern!";
}
else if ($valid_street === 0){
    $error = "Street can only use letters and numbers";
    include('global/error.php');
}
else if ($valid_city === false){
    echo "Error in the pattern!";
}
else if ($valid_city === 0){
    $error = "City can only contain Characters";
    include('global/error.php');
}
else if ($valid_state === false){
    echo "Error in the pattern!";
}
else if ($valid_state === 0){
    $error = "State can only have two letters";
    include('global/error.php');
}
else if ($valid_zip === false){
    echo "Error in the pattern!";
}
else if ($valid_zip === 0){
    $error = "Zip can only have 5 - 9 digits";
    include('global/error.php');
}
else if ($valid_phone === false){
    echo "Error in the pattern!";
}
else if ($valid_phone === 0){
    $error = "Phone can only have 10 digits";
    include('global/error.php');
}
else if ($valid_email === false){
    echo "Error in the pattern!";
}
else if ($valid_email === 0){
    $error = "Email must match the format of: xxxx@xxxx.xxx";
    include('global/error.php');
}
else if ($valid_url === false){
    echo "Error in the pattern!";
}
else if ($valid_url === 0){
    $error = "URL must match the format of https://XXXX.com";
    include('global/error.php');
}
else if ($valid_ytdsales === false){
    echo "Error in the pattern!";
}
else if ($valid_ytdsales === 0){
    $error = "YTD Sales can only use numbers and/or decimals";
    include('global/error.php');
}
else{

require_once('global/connection.php');

$query = 
"UPDATE petstore
SET
pst_name = :pst_name_p,
pst_street = :pst_street_p,
pst_city = :pst_city_p,
pst_state = :pst_state_p,
pst_zip = :pst_zip_p,
pst_phone = :pst_phone_p,
pst_email = :pst_email_p,
pst_url = :pst_url_p,
pst_ytd_sales = :pst_ytd_sales_p, 
pst_notes = :pst_notes_p
WHERE pst_id = :pst_id_p";


try
    {
    $statement = $db->prepare($query);
    $statement->bindParam(':pst_id_p', $pst_id_v);
    $statement->bindParam(':pst_name_p', $pst_name_v);
    $statement->bindParam(':pst_street_p', $pst_street_v);
    $statement->bindParam(':pst_city_p', $pst_city_v);
    $statement->bindParam(':pst_state_p', $pst_state_v);
    $statement->bindParam(':pst_zip_p', $pst_zip_v);
    $statement->bindParam(':pst_phone_p', $pst_phone_v);
    $statement->bindParam(':pst_email_p', $pst_email_v);
    $statement->bindParam(':pst_url_p', $pst_url_v);
    $statement->bindParam(':pst_ytd_sales_p', $pst_ytd_sales_v);
    $statement->bindParam(':pst_notes_p', $pst_notes_v);
    $row_count = $statement->execute();
    $statement->closeCursor();

    //$last_auto_increment_id = $db->lastInsertId();
    }
    catch (PDOException $e)
    {
        $error = $e->getMessage();
        echo $error;
    }
 //include('index.php'); //forwarding is faster, one trip to server
    header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
}
?>
