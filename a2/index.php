<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("../global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> I created an android app that has two pages. One page has the title, picture, and button. When you click the button, it will bring you to the ingredients and directions page.
				</p>

				<h4>Healthly Recipes Homepage</h4>
				<img src="img/main_activity.png" class="img-responsive center-block" alt="Healthy Recipe Homepage">

				<h4>Healthy Recipes Ingredients</h4>
				<img src="img/recipe_activity.png" class="img-responsive center-block" alt="Healthy recipe ingredients and directions">

				
				<?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
