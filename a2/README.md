> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381
# Edit

## Kyle Leombruno


### Assignment #2 Requirements:

*Three parts:*

1. Make the main_activity page
2. Create a button that brings you to the recipe_activity page
3. Make the recipe_activity page

#### README.md file should include the following items:

* Screenshot of main_activity running
* Screenshot of recipe_activity running

> 
>
> #### Git commands w/short descriptions:

1. git init - creates a .git directory in the project directory
2. git status - shows what files have changed
3. git add - tell git what files you want to commit
4. git commit - create the commit object with all the files you added
5. git push - you push files from your local repository to a remote repository
6. git pull - you pull files from a remote repository to your local repository
7. git checkout - change the current branch to a different one

#### Assignment Screenshots:

*Screenshot of running main activity:

![Main activity running](img/main_activity.png)

*Screenshot of running recipe acitvity*:

![Recipe Activity running](img/recipe_activity.png)


#### Tutorial Links:

*Bitbucket lis4381:*
[A2 lis4381 Link](https://bitbucket.org/Kleombruno/lis4381/ "lis4381")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kleombruno/myteamquotes/ "My Team Quotes Tutorial")
