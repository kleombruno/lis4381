<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("../global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> I created an android app that allows you to get the total tickets price based off the band and quantity. I added a drop down menu to choose the band and quantity of tickets, then you click calulate and the you are given the total price.
				</p>

				<h4>Ticket Value Homepage</h4>
				<img src="img/a3.png" class="img-responsive center-block" alt="Ticket Value Homepage">

				<h4>Calculated Ticket Value </h4>
				<img src="img/a32.png" class="img-responsive center-block" alt="Calculated Ticket Value">

      		<h4>SQL Values </h4>
				<img src="img/a3SQL.png" class="img-responsive center-block" alt="SQL Values">
				
				<?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
