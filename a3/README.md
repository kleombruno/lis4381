> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381
# Edit

## Kyle Leombruno


### Assignment #3 Requirements:

*Three parts:*

1. Create a function that calculates the total ticket cost
2. Create a drop down menu that includes four bands
3. Make ERD in SQL

#### README.md file should include the following items:

* Screenshot of MyEvent running
* Screenshot of MySQL ERD

> 
>
> #### Git commands w/short descriptions:

1. git init - creates a .git directory in the project directory
2. git status - shows what files have changed
3. git add - tell git what files you want to commit
4. git commit - create the commit object with all the files you added
5. git push - you push files from your local repository to a remote repository
6. git pull - you pull files from a remote repository to your local repository
7. git checkout - change the current branch to a different one

#### Assignment Screenshots:

*Screenshot of running main activity:

![My Event running](img/A3.png)

*Screenshot of running main activity:

![My Event running](img/a32.png)

*Screenshot of running recipe acitvity*:

![ERD](img/a3SQL.png)


#### Tutorial Links:

*Bitbucket lis4381:*
[A3 lis4381 Link](https://bitbucket.org/Kleombruno/lis4381/ "lis4381")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kleombruno/myteamquotes/ "My Team Quotes Tutorial")
